package com.nova.solutions.junit.mockito.example.junit.util;

import com.nova.solutions.junit.mockito.example.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

@DisplayName("Util test Class.")
public class UtilityTest {

  @DisplayName("4! = 24.")
  @Test
  void factorialNumberTest5() {
    long number = 4;
    Util.factorialNumber(number);
  }
  
  @DisplayName("9! = 362880.")
  @Test
  void factorialNumberTest8() {
    long number = 9;
    Util.factorialNumber(number);
  }

  @DisplayName("Factorial de un numero 'X' en codigo.")
  @ParameterizedTest(name = "{0}! deberia ser {1}.")
  @CsvSource({"1, 1", "4, 24", "9, 362880", "7, 5040", "11, 39916800"})
  void factorialNumberTestX(long number, long result) {
    Util.factorialNumber(number);
  }

  @DisplayName("Factorial de un numero 'X' desde un archivo CSV.")
  @ParameterizedTest(name = "{0}! deberia ser {1}.")
  @CsvFileSource(resources = "/factorial.csv", numLinesToSkip = 1)
  void factorialNumberTestYFromFile(long number, long result) {
    Util.factorialNumber(number);
  }

  @DisplayName("Repetición de test de Factorial")
  @RepeatedTest(value = 3, name = "{displayName} {currentRepetition} de {totalRepetitions}.")
  void factorialNumberRepeated(RepetitionInfo repetitionInfo) {
    Util.factorialNumber(4l);
  }

}
