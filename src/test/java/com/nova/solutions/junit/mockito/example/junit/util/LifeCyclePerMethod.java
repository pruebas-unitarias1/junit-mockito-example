package com.nova.solutions.junit.mockito.example.junit.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@DisplayName("Ciclo de vida de las clases test por método.")
@TestInstance(Lifecycle.PER_METHOD)
public class LifeCyclePerMethod {
  
  @BeforeAll
  static void beforeAll() {
    System.out.println("---------- Antes de todos los Test ----------");
  }
  
  
  @BeforeEach
  void beforeEach() {
    System.out.println("---------- Antes de cada Test ----------");
  }
  
  @Test
  void test() {
    System.out.println("******** Test normal ********");
  }
  
  @RepeatedTest(value = 2)
  void repeatedTest() {
    System.out.println("******** Test repetido ********");
  }
  
  @ParameterizedTest
  @CsvSource({"a, A", "b, B", "c, C"})
  void parameterizedTest(String word, String finalWord) {
    System.out.println("******** Test parametrizado ********");
  }
  
  @AfterEach
  void afterEach() {
    System.out.println("---------- Despues de cada Test ----------");
  }
  
  @AfterAll
  static void afterAll() {
    System.out.println("---------- Despues de todos los Test ----------");
  }
  
}
